import math, sys, random
import numpy as np
import logging
from Error import BatteryDeadError

logger = logging.getLogger('time_sim')

class Battery():

    def __init__(self, capacity_mah, min_usable_capacity=0, stop_early_capacity=0):
        '''
        assuming static 3.3 V, so just represent power in terms of current 
        '''
        self.starting_capacity = capacity_mah
        self.capacity = capacity_mah
        self.min_usable_capacity = min_usable_capacity
        self.battery_life_time_curve = np.zeros((0,2))
        self.stop_early_capacity = stop_early_capacity

    def spend_energy(self, current, time, timestamp):
        '''
        current in Amperes, time in seconds
        '''

        amp_seconds = current*time
        spent_mah = amp_seconds * 1000 / 3600
        self.capacity -= spent_mah

        # new_entry = np.zeros((1,2))
        # new_entry[0,0] = timestamp
        # new_entry[0,1] = self.capacity
        # self.battery_life_time_curve = np.append(self.battery_life_time_curve, new_entry, axis=0) #saving the battery (by appending) KILLS runtime speed... and it's pretty much always going to be a boring linear plot



        if self.capacity < self.min_usable_capacity:
            raise BatteryDeadError("Ding Dong the battery is dead!")
        elif self.capacity <= self.stop_early_capacity:
            raise BatteryDeadError("Enough battery is spent to stop early and extrapolate")

    def spend_energy_mah(self, mah, timestamp):
        
        self.capacity -= mah
        # new_entry = np.zeros((1,2))
        # new_entry[0,0] = timestamp
        # new_entry[0,1] = self.capacity
        # self.battery_life_time_curve = np.append(self.battery_life_time_curve, new_entry, axis=0)

        if self.capacity < self.min_usable_capacity:
            raise BatteryDeadError("Battery is dead!")

    def recharge(self):
        self.capacity = self.starting_capacity

    def deplete(self):
        self.capacity = self.min_usable_capacity
        raise BatteryDeadError("Battery is dead!")