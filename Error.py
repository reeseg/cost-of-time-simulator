

class BatteryDeadError(Exception):
    pass

class SynchronizationFailed(Exception):
    pass