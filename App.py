import sys, math, random
import numpy as np

import logging

logger = logging.getLogger('time_sim')

class Application():

    def __init__(self, W, sync_tolerance, app_current_window=0, app_current_else=0, is_random=False, inter_window_period=3600, do_warm_start=False, name=''):
        '''
        Specify the parameters of an application based on the windows, power usage, and frequency/probability of the windows. By default, windows occur ever 600s
        '''
        self.windows_used = 0
        self.W = W
        self.sync_tolerance = sync_tolerance
        self.app_current_window = app_current_window
        self.app_current_else = app_current_else
        self.name = name

        self.do_warm_start = do_warm_start
        logger.info("Using application with warm start; we will wake up extra early to synchronize the clock")

        self.is_random = is_random
        #rate is windows per second
        if inter_window_period == math.inf or W == math.inf:
            self.inter_window_period = 100
        else:
            self.inter_window_period = inter_window_period #if it is random, then this rate defines the parameter for an exponetial distribution. 


        self.next_window_start = 0
        # self.calculate_next_window_start()
        self.infeasible = False #mark whehter the application was determined to be infeasible (which includes the case where we failed to satisfy its requirements through all time)

    def calculate_next_window_start(self):
        rate = 1/self.inter_window_period

        if self.W == math.inf or np.isinf(self.W):
            self.next_window_start = 1000

        elif self.is_random:
            p = random.random()
            #p = k*exp(-k*t)
            delay = math.log(p/rate)/(-rate)
            self.next_window_start = self.next_window_start + self.W + delay

        else:
            self.next_window_start += self.inter_window_period + self.W

        return self.next_window_start

    def restart_application(self, new_sync_tolerance=-1, new_window_size=-1):
        if new_sync_tolerance > 0:
            self.sync_tolerance = new_sync_tolerance        
        if new_window_size > 0:
            self.W = new_window_size

        self.windows_used = 0
        self.next_window_start = 0
