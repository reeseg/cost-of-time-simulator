import sys, logging, random, math
import numpy as np
import helpers
from Error import SynchronizationFailed

logger = logging.getLogger('time_sim.radio')

class SyncRadio():

    def __init__(self, tx_current, rx_current, tx_time_on_air, rx_time_on_air, bandwidth, sleep_current, tx_rx_time_gap, propagation_time=0, bytes_per_sync_msg=50, sw_nondeterminism_estimate=0):

        self.bandwidth = bandwidth

        self.tx_time = tx_time_on_air
        self.rx_time = rx_time_on_air
        self.tx_rx_time_gap = tx_rx_time_gap

        self.sync_msg_period = self.tx_time + tx_rx_time_gap + 2*propagation_time + self.rx_time #account for rx-time-on-air? don't think so; timestamp refers to the first byte; when it initially leaves/arrives server/client, resp.
        self.propagation_time = propagation_time

        tx_energy = self.tx_time * tx_current #Amperes * seconds
        rx_energy = self.rx_time * rx_current
        standby_energy = sleep_current * (tx_rx_time_gap + 2*self.propagation_time) #assume deterministic gap between TX and RX. We can make this more random later to account for interference, drops, etc., but that has some complex implications on timing uncertainty (per message), overall energy

        # self.single_sync_energy_mah = (tx_energy + rx_energy + standby_energy)
        self.single_sync_avg_power = (tx_energy + rx_energy + standby_energy)/(self.sync_msg_period)

        self.sw_nondeterminism_estimate = sw_nondeterminism_estimate


    def sync(self, env, client_clock, battery, presync=False):
        
        client_times = []
        client_estimates = []
        server_times = []
        sync_times = []

        # server_clock = env

        n_msgs = self.num_messages_for_accuracy(client_clock.sync_tolerance, client_clock, starting_uncertainty=client_clock.abs_error_estimate)

        is_first = client_clock.now < 0 or client_clock.now == math.inf
        if is_first:
            logger.debug('Initial sync clock is off')
            client_clock.turn_on()
            # if client_clock.order_of_rate_adjust >= 1:
            #     n_msgs = max(n_msgs, 2) #add another message to make sure we're on track with the previous estimate of rate. This adds overhead

        logger.debug('Send up to %d synchronization message(s) for this attempt' % n_msgs)
        try:
            for i in range(n_msgs):
                logger.debug('Sync attempt #%d' % (i+1))

                client_time, client_estimate, server_time = yield env.process(self.one_sync(env, client_clock, battery, presync))
                client_times.extend(client_time)
                client_estimates.extend(client_estimate)
                server_times.extend(server_time)
                sync_times.append((len(server_times), server_times[-1]))

                if is_first and i==0:
                    # for the first try, only the last point is good; clock is undefined before then!
                    client_times = [client_times[-1]]
                    client_estimates = [client_estimates[-1]]
                    server_times = [server_times[-1]]

                # else:
                    # helpers.plot_window(client_times, client_estimates, server_times, client_clock.sync_tolerance, 0) 

                battery.spend_energy(self.single_sync_avg_power, self.sync_msg_period, env.now)

                logger.debug(client_clock)


                break_early_condition = client_clock.abs_error_estimate + client_clock.abs_frequency_offset_estimate_ppm * self.sync_msg_period < client_clock.sync_tolerance
                break_early_condition = break_early_condition and ( (not presync) or i>2 )
                if break_early_condition:
                    logger.debug('Sufficiently synchronized; breaking after message %d' % (i+1))
                    break


        except SynchronizationFailed as e:
            logger.warning('Indicate synchronization failure')
            return client_times, client_estimates, server_times, e, sync_times
            

        # helpers.plot_window(client_times, client_estimates, server_times, client_clock.sync_tolerance, -1)

        #return informaion about how time passed and was corrected during this synchronization interval    

        return client_times, client_estimates, server_times, None, sync_times


    def one_sync(self, env, client_clock, battery, presync):

        client_times = []
        client_estimates = []
        server_times = []

        server_clock = env

        t1 = client_clock.get_time()
        # t1_est = client_clock.abs_error_estimate
        # t1_server = server_clock.now

        #suspect
        # ct, ce, st = yield env.process(helpers.wait(self.propagation_time, server_clock, client_clock))
  
        # client_times.extend(ct)
        # client_estimates.extend(ce)
        # server_times.extend(st)

        t2 = server_clock.now + self.propagation_time

        # ct, ce, st = yield env.process(helpers.wait(self.sync_msg_period - 2*self.propagation_time, server_clock, client_clock))        
        ct, ce, st = yield env.process(helpers.wait(self.sync_msg_period, server_clock, client_clock, battery))         #this may be cut short by up to {env.min_step_size}
        client_times.extend(ct)
        client_estimates.extend(ce)
        server_times.extend(st)

        t3 = server_clock.now - self.propagation_time - self.rx_time # start of RX

        # ct, ce, st = yield env.process(helpers.wait(self.propagation_time, server_clock, client_clock))
        # client_times.extend(ct)
        # client_estimates.extend(ce)
        # server_times.extend(st)

        t4 = client_clock.get_time() - self.rx_time #RX message first arrives
        # t4_est = client_clock.abs_error_estimate
        # t4_server = env.now

        logger.debug('t1 %f, t2 %f, t3 %f, t4 %f' % (t1,t2,t3,t4))

        offset = ((t2-t1) + (t3-t4))/2
        delay = (t4-t1) - (t3-t2)
        # logger.debug('Offset %f, and Delay is %f whereas 2*t_prop is %f' % (offset, delay, 2*self.propagation_time))
        client_clock.rate_adjust(server_clock, t1, t2, t3, t4, offset, delay, presync)


        ##suspect implementation
        logger.debug('Before adding offset: %.5f'% client_clock.get_time())
        # client_clock.set_time(t4 + offset) #hard correction; not spread over time like NTP. Fine. Or just directly set the clock to whatever the serveer is?
        # # print(client_clock.now)
        # # client_clock.now = t4
        # client_clock.abs_error_estimate = self.sync_uncertainty(client_clock.oscillator.rated_frequency) #conservative; combining the measurements may give better estimate, esp. if starting from a low error estimate
        client_clock.apply_sync_offset(offset, self.sync_uncertainty(client_clock.oscillator.rated_frequency))

        ##
        # client_time_corrected = client_clock.get_time()
        client_time_corrected = client_clock.get_time()
        client_time_corrected_est = client_clock.abs_error_estimate


        logger.debug('After adding offset: %f' % client_clock.get_time())
        logger.debug('Server time: %f' % server_clock.now)

        client_times.append(client_time_corrected)
        client_estimates.append(client_time_corrected_est)
        server_times.append(server_clock.now)

        return client_times, client_estimates, server_times


    def sync_uncertainty(self, osc_frequency):
        #upper bound
        uncertainty  = 2/osc_frequency + 1/self.bandwidth + self.sw_nondeterminism_estimate
        return uncertainty


    def num_messages_for_accuracy(self, accuracy, client_clock, starting_uncertainty=math.inf):
        #assume for now that the oscillator model (both learned and true) are stationary throughout this period of time

        logger.debug('starting_uncertainty for finding number of sync messages to exchange: %e' % starting_uncertainty)
        best_ppm_est = client_clock.abs_frequency_offset_estimate_ppm
        logger.debug('calculate number of messages, with starting ppm estimate %.5e' % best_ppm_est)

        n=0
        # max_n = math.floor(client_clock.inter_window_period/self.sync_msg_period)
        delta = starting_uncertainty
        while delta + best_ppm_est*self.sync_msg_period > client_clock.sync_tolerance:
            # print(delta)
            previous_delta=delta
            delta += best_ppm_est*self.sync_msg_period #accumulate some error over the synchronization exchange
            delta = 1/math.sqrt(1/delta**2 + 1/self.sync_uncertainty(client_clock.oscillator.rated_frequency)**2) #an optimal estimator would be able to estimate this with a standard deviation (delta) based on the aggregation of these two. This is based on a 2-sample CRLB; the N-sample CRLB is not analytically succinct (requires recursive/iterative solution), because the uncertainty increases during each synchronization message. Not this these calculations are more focused on frequency-based variations than the actual offset itself. This also does not account for any rate adjustment/FLL control parameters
            if abs(previous_delta - delta) < client_clock.sync_tolerance*1e-5 and n>100:
                logger.warning('Synchronization tolerance is unachievable, requiring infinite number of windows! We have found the fixpoint (i.e. minimum achievable error estimate) to be %e' % delta)

                return math.inf

            n += 1
            if client_clock.order_of_rate_adjust > 0 and n>1:
                #TODO; this isn't quite right; it takes longer for the control system to converge than this will allow, so we may need those parameters to calculate the minimum 

                #should improve if we've had a chance to do rate-adjustment
                best_ppm_est = 1/(self.sync_msg_period * client_clock.oscillator.rated_frequency) #based on the clock resolution and duration of time passed between synchronization message

        n = max(1,n)


        logger.debug(f'Estimate it would take up to {n} messages(s) to synchronize')

        return n
