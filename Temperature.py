import sys, math, random, logging
from scipy import interpolate
from enum import Enum
from matplotlib import pyplot as plt
import numpy as np
Polynomial = np.polynomial.polynomial.Polynomial


logger = logging.getLogger('time_sim.temperature')

class TemperatureSensor():
    '''
    Base on SiLab S7006-A20
    '''

    def __init__(self, temperature_cycle):
        self.temperature_cycle = temperature_cycle
        self.read_duration = 7e-3
        self.on_current = 100e-6
        self.temperature_uncertainty = .5 #degrees C


    def read(self, time, noisey=False):
        #assume periodic temperature, e.g. diurnal cycle
        while time < self.temperature_cycle.start_time or time > self.temperature_cycle.stop_time:
            if time < self.temperature_cycle.start_time:
                time += (self.temperature_cycle.stop_time - self.temperature_cycle.start_time)
            else:
                time -= (self.temperature_cycle.stop_time - self.temperature_cycle.start_time)

        noise = 0
        if noisey:
            noise = random.gauss(0, 1) * self.temperature_uncertainty
        return float(self.temperature_cycle.temperature_function(time)) + noise

    def __call__(self, time, noisey=False):
        return self.read(time, noisey)

class TemperatureCycle():

    def __init__(self, start_time, stop_time, temperature_function):
        if temperature_function(start_time) - temperature_function(stop_time) > 1e-4:
            raise ValueError('Temperature Function is not smooth enough!')

        self.start_time = start_time
        self.stop_time = stop_time
        self.temperature_function = temperature_function

    @staticmethod
    def diurnalRoadSurface():
        #source for numbers https://climateaudit.org/2007/08/05/more-on-asphalt/
        hr = 3600.
        points = np.asarray([[0,93], [hr, 90], [hr*2, 88], [hr*3, 86], [hr*4, 84], [hr*5, 82], [hr*6, 83], [hr*7, 100], [hr*8, 112], [hr*9, 125], [hr*10, 135], [hr*11, 146], [hr*12, 150], [hr*13, 154], [hr*14, 158], [hr*15, 152], [hr*16, 145], [hr*17, 135], [hr*18, 128], [hr*19, 122], [hr*20, 112], [hr*21, 109], [hr*22, 105], [hr*23, 98], [hr*24, 93]])
        
        points[:,1] = (points[:,1] -32) * 5/9 #convert to celsius 
        func = interpolate.interp1d(points[:,0], points[:,1], kind='cubic')


        return TemperatureCycle(points[0,0], points[-1, 0], func)

class TemperatureCurve():

    def __init__(self, order, coefficients, use_window_param=False):
        '''
        coefficients used for numpy's Polynomial class, which takes arguments in increasing order

        Call as CurveObj(temperature) to get the frequency error in ppm

        '''

        self.order = order
        if self.order == 2:
            self.type = 'quadratic'
            assert len(coefficients == 3)
        elif self.order == 3:
            self.type = 'cubic'
            assert len(coefficients == 4)
        else:
            self.type = 'unknown-type'

        self.coefficients = coefficients

        if use_window_param:
            self.temperature_curve = Polynomial(coefficients, domain=[-60,100], window=[-60, 100])
        else:
            self.temperature_curve = Polynomial(coefficients, domain=[-60,100])

    def __call__(self, temperature):

        return self.temperature_curve(temperature)

    @staticmethod 
    def generateQuadraticCurve(inflection_point = 25, quadratic_param = -0.04, allow_random=False):
        '''
        pull some params from abracon tuning-fork crystal : https://abracon.com/Resonators/ABS07W.pdf
        '''
        if allow_random:
            inflection_point = inflection_point + (random.random()-1/2)*8 # +-4 degrees
            quadratic_param = quadratic_param + (random.random()-1/2) * .01 #+- .005
            logger.info('Adding randomness to the temperature curve!!')
            # TODO: add noise
            
        quadratic_param *= 1e-6 #Correct to be in PPM

        coeff0 = (-inflection_point)**2
        coeff1 = 2 * (-inflection_point)
        coeff2 = 1

        coefficients = quadratic_param * np.asarray([coeff0, coeff1, coeff2]) 
        return TemperatureCurve(2, coefficients=coefficients, use_window_param=True)


    @staticmethod 
    def generateCubicCurve15min(allow_random=False, xstretch=1, xshift =0, ystretch = 1, yshift = 0):
        '''
        use the stretch and shift params to perturb the curve
        '''
        # how to do this...nominally do for 35 degrees, 15 minutes

        if allow_random:
            xshift = xshift + (random.random()*-1/2) *4 # shift up to 2 degrees left or right
            xstretch = xstretch + (random.random()-1/2)*.1
            yshift = yshift + (random.random()-1/2) * 6  #shift up to 3 ppm up or down
            ystretch = ystretch+(random.random()-1/2)*.1

        points = np.asarray([[-60, -30], [-50, -17], [-40, -5], [-35, 0],[-30, 6], [-18, 8], [0,6], [20, 0], [30, -4], [40, -6], [50, -10], [60, -10], [70,6], [80, 3], [90, 15], [100, 30]], dtype='float64')

        points[:,0] = points[:,0] * xstretch + xshift
        points[:,1] = (points[:,1] * ystretch + yshift)*1.e-6

        func = Polynomial.fit(points[:,0], points[:,1], 3, domain=[-60,100])
 


        return TemperatureCurve(3, func.coef, use_window_param=False)

        # return TemperatureCurve(3, [ -3.48406117e-06, -2.88434920e-05,  1.33825686e-05,  4.91961023e-05,])

        # return func, interpolated