##big sim data generator
import os, sys, math, logging, pickle
import simpy
import numpy as np
import random
from matplotlib import pyplot as plt


from Client import ClientClock
from Battery import Battery
from App import Application
from Temperature import TemperatureCycle, TemperatureSensor, TemperatureCurve
from Oscillator import Oscillator
from Radio import SyncRadio
import helpers
import time_simulator
from Error import BatteryDeadError, SynchronizationFailed

logger = logging.getLogger('time_sim.bigdata')


def generate_data(osc, radio, battery, order_of_rate_adjust, sync_tol_arr, window_size_arr, max_time=math.inf):
    ## The whole shebang. This is a lot of data, and requires a lot of time to run properly
    time_simulator.set_logger_levels(logging.DEBUG)
    time_simulator.set_file_logger_level(logging.INFO)
    time_simulator.set_console_logger_level(logging.WARNING)

    # sync_tol_arr = np.asarray([1e-3])
    battery_life_expected = np.zeros((sync_tol_arr.shape[0], window_size_arr.shape[0]))
    energy_per_window_arr = np.zeros((sync_tol_arr.shape[0], window_size_arr.shape[0]))

    for j, w in enumerate(window_size_arr):
        for i, sync_tol in enumerate(sync_tol_arr):
            print()
            inter_window_period = max(100, 2*w)
            app = Application(w, sync_tol, inter_window_period = inter_window_period, do_warm_start=True)
            battery.recharge() 

            env = simpy.Environment()
            # sim_process = env.process(run_sim(env, app, osc_high_f_xtal, lora_radio, battery, plot_n_windows=0, max_windows=50, steps_per_second=100, order_of_rate_adjust=0))
            sim_process = env.process(time_simulator.run_sim(env, app, osc, radio, battery, plot_n_windows=0, max_windows=math.inf, steps_per_second=500, order_of_rate_adjust=order_of_rate_adjust, max_time=max_time))
            try:
                logger.critical(f'Run sim for: Sync tol= {sync_tol} \tand window size= {w}\t{order_of_rate_adjust}th order rate-adjustment')
                env.run(until=sim_process)
                # x, y, z = yield sim_process
                logger.warning('\n\nSimulation exited')
            except (SynchronizationFailed) as e: #used for telling that the battery has died or exit for some other valid reason
                logger.error(e)
                logger.warning(f'Sync tol was {sync_tol} and window size was {w}')

            except (BatteryDeadError):
                logger.info('Battery dead/depleted')

            except BaseException as e:
                logger.error(e)
                logger.warning(f'Sync tol was {sync_tol} and window size was {w}')
                raise e

            extrapolated_lifetime, average_energy_per_window = helpers.extrapolate_battery_life(battery, app, env.now, only_include_useful_life=True)
            battery_life_expected[i][j] = extrapolated_lifetime
            energy_per_window_arr[i][j] = average_energy_per_window
            logger.info(f"Results for sync tolerance {sync_tol}")
            logger.info(f"Estimated battery lifetime: {extrapolated_lifetime} days")
            logger.info(f"Energy per window: {average_energy_per_window} mAh\n")

    outdata = {'sync_tol':sync_tol_arr, 'window_size':window_size_arr, 'battery_life':battery_life_expected, 'energy_per_window':energy_per_window_arr}
    filename = f'./data_store/datamap_fixedTemperature_RA={order_of_rate_adjust}.pickle'
    outfile = open(filename, 'wb+')
    pickle.dump(outdata, outfile)
    outfile.close()


def main():

    # osc_low_f_xtal = Oscillator(80-6, 32768, 1.5e-6, temperature_vs_drift_model='quadratic') #quadratic model; tuning fork
    # osc_low_f_xtal_ctrl = Oscillator(80e-6, 32768, 1.5e-6, temperature_vs_drift_model='quadratic', allow_random=False)
    osc_med_f_xtal = Oscillator(20e-6, 12e6, 300e-6, temperature_vs_drift_model='cubic') #cubic model; assume AT cut 35˚15' centered around 20 C
    # osc_high_f_xtal = Oscillator(20e-6, 40e6, 2.5e-3, temperature_vs_drift_model='cubic') #cubic temperature model; also AT cut

    dist_from_gateway = 1000
    t_prop = dist_from_gateway / 3e8
    lora_radio = SyncRadio(80e-3, 5e-3, 600e-3, 150e-3, 500e3, 600e-9, 1, propagation_time=t_prop) #1 second delay for the sake of LoRaWAN, but not required...

    # test_application = Application(60, 1e-3, inter_window_period=3600)
    # app = test_application
    battery = Battery(1000, stop_early_capacity=985)

    max_time = 1e7 #no more than 10M seconds; that's already >100 days

    ## big set
    sync_tol_arr = np.logspace(-6,0, num=19)
    window_size_arr = np.logspace(0, 3, num=10)
    ## small set
    # sync_tol_arr = np.logspace(-6,0, num=3)
    # window_size_arr = np.logspace(-1, 1, num=2)

    window_size_arr = np.append(window_size_arr, math.inf)

    rate_adjustment = [2]
    for ra in rate_adjustment:
        generate_data(osc_med_f_xtal, lora_radio, battery, ra, sync_tol_arr, window_size_arr, max_time = max_time)
    # ra=1
    # generate_data(osc_med_f_xtal, lora_radio, battery, ra, sync_tol_arr, window_size_arr


if __name__ == "__main__":
    main()