import sys, random, math
import numpy as np
import logging

from Temperature import TemperatureCurve

logger = logging.getLogger('time_sim.oscillator')

class Oscillator():

    def __init__(self, rated_stability, rated_frequency, on_current, real_frequency_offset=0, temperature_vs_drift_model=None, allow_random=True, allow_random_temp_curve=False, best_case_frequency_error=7.5e-9):

        self.rated_stability = rated_stability #in parts; usually ppm (base 1e-6)
        self.rated_frequency = rated_frequency #Hz; we can only measure to precision of 1/frequency. It is supposed to run at this frequency
        self.min_resolution = 1/self.rated_frequency
        self.on_current = on_current
        # self.best_case_frequency_error = best_case_frequency_error
        self.best_case_frequency_error=rated_stability/2000


        self.real_frequency_offset = real_frequency_offset

        if allow_random:
            random_offset = random.gauss(0, self.rated_stability/3) * self.rated_frequency
            if random_offset > self.rated_stability*self.rated_frequency:
                random_offset = self.rated_frequency * self.rated_stability * abs(random_offset)/random_offset
                logger.warning('random offset was too much; rounding to rated stability')
            self.real_frequency_offset += random_offset
            logger.debug('Frequency offset for oscillator is %f' % self.real_frequency_offset)

        self.static_frequency_offset = self.real_frequency_offset #save for later
            
        self.real_frequency = self.rated_frequency + self.real_frequency_offset
        logger.debug('Actual oscillator frequency is %f rather than rated %f' % (self.real_frequency, self.rated_frequency))


        #Model for the drift rate with time. This may be subject to random noise.
        self.temperature_vs_drift_model = temperature_vs_drift_model
        if temperature_vs_drift_model == 'quadratic':
            
            self.temperature_model = TemperatureCurve.generateQuadraticCurve(allow_random=allow_random_temp_curve)
        elif temperature_vs_drift_model == 'cubic':
            self.temperature_model = TemperatureCurve.generateCubicCurve15min(allow_random=allow_random_temp_curve)
        else:
            self.temperature_model = TemperatureCurve(None, [0]) # no temperature dependence! Degenerate case
        
        self.temperature_based_offset = 0 #assume we started from room temperature


 
    def current_frequency(self, time):
        return self.real_frequency


    def randomize_offset(self):
        random_offset = random.gauss(0, self.rated_stability/3) * self.rated_frequency
        if random_offset > self.rated_stability*self.rated_frequency:
            random_offset = self.rated_stability * abs(random_offset)/random_offset
            logger.warning('random offset was too much; rounding to rated stability')
        self.real_frequency_offset = random_offset
        logger.debug('Frequency offset for oscillator is %f' % self.real_frequency_offset)

    def new_frequency_error(self):
        self.randomize_offset()
        self.real_frequency = self.real_frequency_offset + self.rated_frequency
        logger.debug('Updated the frequency offset to %f' % self.real_frequency_offset )

    def adjust_by_temp(self, new_temp):

        new_offset_ppm = self.temperature_model(new_temp)
        # logger.debug(f'error {new_offset_ppm} in parts from temperature {new_temp}')
        self.temperature_based_offset = new_offset_ppm * self.rated_frequency

        self.real_frequency_offset = self.static_frequency_offset + self.temperature_based_offset
        self.real_frequency = self.rated_frequency + self.real_frequency_offset
        # logger.debug(f'New frequnecy is {self.real_frequency}')

        return new_offset_ppm
