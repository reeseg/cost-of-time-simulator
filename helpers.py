###helpers

import sys, logging, math, copy
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter


logger = logging.getLogger('time_sim.helpers')

def wait(delay_until, env, client_clock, battery, sooner_than_later=True):

    client_times = []
    client_estimates = []
    server_times = []

    server_clock = env
    starting_time = server_clock.now

    if delay_until < 0:
        raise ValueError('Wait-until delay is a negative value! Quick Inspector, we have to get out of when!')

    logger.debug('Delaying for %e time' % delay_until)

    while delay_until > 0:
        if delay_until > env.min_step_size:

            yield env.timeout(env.min_step_size)
            client_clock.time_step(env.min_step_size)

            client_times.append(client_clock.get_time())
            client_estimates.append(client_clock.abs_error_estimate)
            server_times.append(server_clock.now)

            delay_until -= env.min_step_size

        if sooner_than_later and delay_until <= env.min_step_size:
            break
        

        # else:

        #     yield env.timeout(delay_until)
        #     client_clock.time_step(delay_until)

        #     client_times.append(client_clock.get_time())
        #     client_estimates.append(client_clock.abs_error_estimate)
        #     server_times.append(server_clock.now)

        #     delay_until -= delay_until
        #     break

    battery.spend_energy(server_clock.now-starting_time, client_clock.oscillator.on_current + client_clock.app.app_current_window, server_clock.now)

    return client_times, client_estimates, server_times

import matplotlib.ticker as mtick

def plot_window(client_time, client_estimate, server_time, sync_tolerance, window_count, window_start_time=None, save_filename=None):
        ct_win = np.asarray(client_time)
        ce_win = np.asarray(client_estimate)
        ce_win_reversed = ce_win * -1
        st_win = np.asarray(server_time)

        tolerance_line = np.ones(st_win.shape) * sync_tolerance

        #client - server
        delta_t = st_win - ct_win

        # print_arr = np.asarray([st_win,ct_win, delta_t])
        # print_arr[:2,:] = print_arr[:2,:] - st_win[-1]
        # np.set_printoptions(precision=12)


        _, ax = plt.subplots()
        plt.plot(st_win, delta_t, 'b.', label='∆t')
        plt.plot(st_win, ce_win, 'r-', label='Error Estimate')
        plt.plot(st_win, ce_win_reversed, 'r-')
        line1, = ax.plot(st_win, tolerance_line, 'k--', label='Sync Accuracy')
        dashes = [15, 5]  # 15 points on, 5 off
        line1.set_dashes(dashes)
        line2, = ax.plot(st_win, tolerance_line*-1, 'k--')
        line2.set_dashes(dashes)

        if window_start_time != None:
            plt.axvline(window_start_time, color='grey', label='Window Start')

        title = "Drift-o-gram for Window %d at Time Acc=%.2e seconds" % (window_count, sync_tolerance)
        title = "Drift-o-gram for Time Accuracy=%.2e seconds" % sync_tolerance #FIXME: remove after gifsor
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.2e'))

        plt.title(title)
        plt.xlabel('Time (s)')
        plt.ylabel('Clock Error ∆t (s)')
        # plt.legend()
        # ax.legend(bbox_to_anchor=(1,0.7), loc='upper left')
        ax.legend(loc='lower right')
        # plt.subplots_adjust(right=0.7)
        plt.subplots_adjust(left=.17)
        # ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
        if save_filename:
            plt.savefig(save_filename)
        else:
            plt.show()


        # plt.subplots()
        # plt.plot(st_win, st_win, 'b')
        # plt.plot(st_win, ct_win, 'r')
        # plt.show()

from matplotlib.pyplot import cm
from matplotlib.colors import BoundaryNorm, LogNorm, LinearSegmentedColormap, ListedColormap
from matplotlib.ticker import MaxNLocator, LogFormatter
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D

def colormap_battery_power(x,y,values, application_points=[], rate_adjustment=0, normalization_params=None, as_num_windows=False):
    x = np.copy(x)
    x = x[:-1] #remove infinity
    # x[-1] = 10000
    y = np.copy(y)
    values = np.copy(values)

    infeasible_points = values==0
    smallest = np.min(values[np.logical_not(infeasible_points)])

    values = np.ma.masked_where(infeasible_points, values)

    # values[infeasible_points] = smallest

            
    inf_values = values[:,-1]
    values = values[:,:-1]

    if as_num_windows:
        values = values*86400/x
        inf_values[:] = 1
        smallest = np.min(values)
        print(smallest)

    if normalization_params != None and len(normalization_params)==2:
        min_val = normalization_params[0]
        max_val = normalization_params[1]

    else:
        max_val = np.max(values)
        min_val = smallest
        

    if np.any(np.isnan(values)):
        logger.warning('NaN values for the color map, oh no!')


    # cmap = copy.copy(plt.get_cmap('OrRd'))
    cmap = LinearSegmentedColormap.from_list('mycmap', ['firebrick', 'yellow', 'green'])
    cmap.set_bad((0,0,0,0.5))
    cmap_inf = copy.copy(cmap)
    cmap_inf.set_under('w')

    norm = LogNorm(vmin=min_val, vmax=max_val, clip=False)

    if not as_num_windows:
        fig, (ax, ax_split) = plt.subplots(1,2,sharey=True, gridspec_kw={'width_ratios':[4.5,1]})
        fig.subplots_adjust(wspace=0)

    else: 
        fig, ax = plt.subplots()


    im = ax.pcolormesh(x, y, values, cmap=cmap, shading='auto', norm=norm)
    ax.loglog()
    ax.set_xlim(x[0], x[-1])
    ax.set_ylabel('Synchronization Tolerance (s)')
    ax.set_ylim(y[0], y[-1])
    ax.set_xlabel('Synchronization Window Size W (s)')


    if not as_num_windows:
        ## Draw the infinite-window size values
        # inf_values_plot = np.log10(np.asarray([inf_values, inf_values]).T)
        zz = np.zeros(inf_values.shape) + 1e-10
        inf_values_plot = np.asarray([zz, zz, inf_values]).T

        ax_split.pcolormesh([0,1,2], y, inf_values_plot, cmap=cmap_inf, shading='auto', norm=norm)

        #format the two plots to be squeezed together
        ax_split.tick_params(axis="y", left=False, labelleft=False)
        ax.spines['right'].set_visible(False)
        ax_split.spines['left'].set_visible(False)
        # ax_split.yaxis.tick_left()
        ax_split.tick_params(left=False, labelleft=False, labelsize=16)
        ax_split.yaxis.set_visible(False)
        ax_split.xaxis.set_ticks([2]) #the middle; it thinks we have pixels for 0 and 1
        ax_split.xaxis.set_ticklabels(['\u221e'])

        ## Draw diagonals to split the axis between finite and infinite window size plots
        kwargs = dict(transform=ax_split.transAxes, color='k', clip_on=False)  # switch to the bottom axes
        ax_split.plot((.175, .6), (-.025, .02), **kwargs)  # bottom-left diagonal
        ax_split.plot((.175, .6), (1-.025, 1+.02), **kwargs)  # bottom-left diagonal
        ax_split.axvline(x=1.5)

    # ax_split.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal

    # ax.yaxis.tick_right()

    ## Add application points
    for a in application_points:
        w = a.W
        sync = a.sync_tolerance
        name = ''
        for i, n in enumerate(a.name.split()):
            if i == len(a.name.split())-1:
                name += n
            else:
                name += n+'\n'

        if w == math.inf and not as_num_windows:
            ax_split.plot(2, sync, 'o', color='turquoise')
            props = {'width':3, 'frac':.5, 'headwidth':8,'shrink':5, 'color':'blue'}
            ax_split.annotate(name, (2,sync),xytext=(-10,sync/5))
            ax_split.annotate('', (1.8,sync/1.35), xytext=(-2.5, sync/3), arrowprops=props)
        else:
            ax.plot(w, sync, 'o', color='turquoise')
            ax.annotate(name, (w,sync),xytext=(w/2.5,sync*2))

    # Add color bar
    formatter = LogFormatter(base=10, labelOnlyBase=False)
    if as_num_windows:
        colorbar_label = 'Number of Syncrhonization Windows'
        fig.colorbar(im, label=colorbar_label, format=formatter, orientation='vertical')

    else:
        colorbar_label = 'Useful Battery Life (days)'
        fig.colorbar(im, label=colorbar_label, format=formatter, orientation='vertical', fraction = .5, pad=.1)

    legend_element = Line2D([0], [0], color='w', marker='s', label='Infeasible\nConfigurations', markerfacecolor=(0,0,0,0.5), markersize=20)

    if as_num_windows:
        title = 'Number of Synchronization Windows for \n'
    else:
        title = 'Useful Battery Life for '
    if rate_adjustment == 0:
        title += '0th'
    elif rate_adjustment == 1:
        title += "1st"
    elif rate_adjustment == 2:
        title += '2nd'
    else:
        raise Exception('what order rate adjustment did you say?')

    title += ' Order Rate Adjustment'


    ax.set_title(title)
    ax.legend(handles=[legend_element], loc='upper left', bbox_to_anchor=(1.1,0))
    plt.show()




def color_map_energy_per_window(x, y, values, normalized=False, application_points=[]):
    x = np.copy(x)
    y = np.copy(y)
    x[np.isinf(x)] = np.max(x[np.logical_not(np.isinf(x))]) * 10 #infinite values need to be set to something
    y[np.isinf(y)] = np.max(y) * 10 #infinite values need to be set to something

    values = np.copy(values) # if something changes, I need to make sure it doesn't change the original. Damned references
    
    # set any infinite values to some finite number that is still higher, but not ruinuous to the relative values in the plot

    infeasible_points = np.isinf(values) #TODO: identify these cells and mark cells as totally black

    values[np.isinf(values)] = np.max(values[np.logical_not(infeasible_points)]) * 2
    values[values == 0] = np.min(values[values>0])/10

    # logx = np.log(x)
    # logy = np.log(y)
    # dx = logx[1]-logx[0]
    # dy = logy[1]-logy[0]

    # values = np.log10(values)
    # print('logged')
    print(values)
    print(type(values))
    print(np.log10(values))

    cmap = plt.get_cmap('OrRd')

    # levels = MaxNLocator(nbins=logx.shape[0]*logy.shpae[0]).tick_values(values.min(), values.max())
    levels = MaxNLocator(nbins=20).tick_values(values.min(), values.max())
    norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)
    norm = LogNorm(vmin=values.min(), vmax=values.max(), clip=True)
        # (levels, ncolors=cmap.N, clip=True)

    fig, ax = plt.subplots()
    im = ax.pcolormesh(x, y, values, cmap=cmap, shading='auto', norm=norm)
    ax.loglog()
    ax.set_xlim(x[0], x[-1])
    ax.set_ylabel('Synchronization Tolerance (s)')
    ax.set_ylim(y[0], y[-1])
    ax.set_xlabel('Synchronization Window Size W (s)')
    
    formatter = LogFormatter(10, labelOnlyBase=False)

    colorbar_label = 'Battery usage per window (mAh)'
    title = 'Heatmap of Energy per Window'


    if normalized:
        colorbar_label = 'Battery usage per window, normalized by duratin (mAh/W)'
        title='Heatmap Normalized to Window Size'


    fig.colorbar(im, label=colorbar_label, format=formatter)

    plt.title(title)
    plt.show()

    ## As contour plot. Not that interesting
    # x,y = np.meshgrid(logx,logy)
    # values = np.copy(np.log10(values))
    # fig, ax = plt.subplots()
    # ax.contourf(x + dx/2., y + dy/2., values,  cmap=cmap)
    # # ax.set_xlim(x[0], x[-1])
    # ax.set_ylabel('Synchronization Tolerance (s)')
    # # ax.set_ylim(y[0], y[-1])
    # ax.set_xlabel('Synchronization Window Size W (s)')
    # formatter.label_minor(True)
    # fig.colorbar(im, label=colorbar_label, format=formatter)

    # plt.title(title)
    # plt.show()


def plot_battery_usage_curve_vs_tol(sync_tol_arr, battery_life_expected, energy_per_window_arr=None, max_capacity=1000, applications=[]):

    # logger.debug(sync_tol_arr)
    # logger.debug(battery_life_expected)
    # logger.debug(energy_per_window_arr)
    zero = battery_life_expected == 0
    battery_life_expected = battery_life_expected[np.logical_not(zero)]
    sync_tol_arr = sync_tol_arr[np.logical_not(zero)]

    _, ax1 = plt.subplots()

    ax1.loglog(sync_tol_arr, battery_life_expected, 'r-')
    ax1.set_xlabel('Synchronization Accuracy (s)')
    ax1.set_ylabel(f'Battery Life of {max_capacity} mAh battery (days)')
    if energy_per_window_arr:
        ax2 = ax1.twinx()
        ax2.set_ylabel('Battery consumption per Window (mAh)') #yes, I know that mAh is units of charge and not actual energy. Let's not be overly pedantic
        plt.subplots_adjust(right=0.8)

        ax2.loglog(sync_tol_arr, energy_per_window_arr, 'b-')

    for app in applications:
        # w = app.W
        sync = app.sync_tolerance
        name = app.name.split()[0] + '\n' + app.name.split()[1]

        life = battery_life_expected[np.where(sync_tol_arr==sync)]
        print(life)
        if life.shape ==(0,):
            print('here')
            #find nearest point on the plot
            above = np.where(sync_tol_arr > sync)
            below = np.where(sync_tol_arr < sync)
            print(above)
            print(below)
            if above.shape==(0,) or below.shape==(0,):
                continue

            lower = below[-1]
            upper = above[0]
            life = 10**((np.log10(battery_life_expected[upper])+np.log10(battery_life_expected[lower]))/2) 
            print(battery_life_expected[upper])
            print(battery_life_expected[lower])
            print(life)
        else: life = life[0]

        ax1.plot(sync, life, 'o', color='black')
        # props = {'width':3, 'frac':.5, 'headwidth':8,'shrink':5, 'color':'blue'}
        ax1.annotate(name, (sync,life))
        # ax1.annotate('', (1.8,sync/1.35), xytext=(-3, sync/4), arrowprops=props)
        # ax1.annotate('', (1.8,sync/1.35), xytext=(-3, sync/4))


    plt.show()

def plot_where_power_goes(sync_tol_arr, battery_life, osc, capacity = 1000):

    osc_current = osc.on_current
    capacity_As = capacity * 3600/1000

    zero = battery_life == 0
    battery_life = np.copy(battery_life[np.logical_not(zero)])
    sync_tol_arr = sync_tol_arr[np.logical_not(zero)]

    osc_percentage = np.zeros(battery_life.shape)

    for i, bl in enumerate(battery_life):
        # sync_tol = sync_tol_arr[i]
        osc_percentage[i] = bl * 86400 * osc_current / capacity_As

    radio_percentage = 1-osc_percentage

    # print(radio_percentage)

    _, ax2 = plt.subplots()

    ax2.set_ylabel('Percentage of battery') 
    _ = ax2.bar(sync_tol_arr[:-1], osc_percentage[:-1], width=np.diff(sync_tol_arr), ec='k', align='edge' ,color='yellow',label='Oscillator')
    ax2.set_xscale('log')
    _ = ax2.bar(sync_tol_arr[:-1], radio_percentage[:-1], width=np.diff(sync_tol_arr), bottom=osc_percentage[:-1],ec='k', align='edge' ,label='Radio')

    ax1 = ax2.twinx()

    _ = ax1.loglog(sync_tol_arr, battery_life, '-',color='black',linewidth=5, label='Battery Life')
    ax1.set_xlabel('Synchronization Accuracy (s)')
    ax1.set_ylabel(f'Battery Life of {capacity} mAh battery (days)')
    ax1.yaxis.set_ticks_position('left')
    ax1.yaxis.set_label_position('left')
    ax2.yaxis.set_ticks_position('right')
    ax2.yaxis.set_label_position('right')


    lines1, labels1 = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()

    ax2.legend(lines1+lines2, labels1+labels2, loc='lower right')
    # ax1.legend(loc='right')

    plt.xlabel('Synchronization Accuracy')
    plt.title('Battery Usage vs. Synchronization Accuracy')




def plot_num_windows_curves(window_sizes, values, max_capacity=1000, sync_tol=None):


        _, ax1 = plt.subplots()
        colors = ['r','black','b']

        for i, value_set in enumerate(values):


            if i==0:
                label= '0th Order Rate Adjust'
            elif i==1:
                label = '1st Order Rate Adjust'
            elif i==2: 
                label = '2nd Order Rate Adjust'
            else:
                label = '%dth Order Rate Adjust' % i


            ax1.loglog(window_sizes, value_set, colors[i], label=label)
            ax1.set_xlabel('Window Size (s)')
            ax1.set_ylabel(f'Number of Windows for {max_capacity} mAh battery')

            title = 'Max Window Count for Rate Adjustment Strategies'
            plt.legend(loc='upper right')



        if sync_tol and not sync_tol == math.inf:
            title += '\nFor Synchronization Tolerance %.1e seconds' % sync_tol

        plt.title(title)
        
        plt.show()


def plot_battery_usage_curves(sync_tol_arr, values, max_capacity=1000, window_size=None, show_avg_power=False):


        _, ax1 = plt.subplots()
        if show_avg_power:
            ax2=ax1.twinx()
            ax2.set_ylabel('Average power throughout window (W)')


        colors = ['r','black','b']

        for i, value_set in enumerate(values):


            if i==0:
                label='0th Order Rate Adjust'
            elif i==1:
                label = '1st Order Rate Adjust'
            elif i==2: 
                label = '2nd Order Rate Adjust'
            else:
                label = '%dth Order Rate Adjust' % i


            ax1.loglog(sync_tol_arr, value_set, colors[i], label=label)
            ax1.set_xlabel('Synchronization Accuracy (s)')
            ax1.set_ylabel(f'Battery Life of {max_capacity} mAh battery (days)')


            title = 'Battery Life Estimates vs. Accuracy'

            if show_avg_power:

                power = max_capacity*3.3*3.6 / (value_set * 86400)
                print(power)
                ax2.loglog(sync_tol_arr, power, colors[i])


        # if window_size and not window_size == math.inf:
        #     title += '\nWindow Size = %.0f seconds' % window_size
        # elif window_size and window_size == math.inf:
        #     title += '\nWindow Size = \u221e seconds'
        if window_size and not window_size == math.inf:
            title += '\nActive Period $\mathregular{T_{on}}$ = %.0f seconds' % window_size
        elif window_size and window_size == math.inf:
            title += ' - No Duty Cycling'

        formatter = LogFormatter(labelOnlyBase=False, minor_thresholds=(3, 0.4))
        # formatter = LogFormatter(labelOnlyBase=False)
        ax1.yaxis.set_minor_formatter(formatter)
        ax1.yaxis.set_major_formatter(formatter)

        plt.title(title)
        ax1.legend(loc='right')

        plt.show()

def plot_battery_usage_curves_duty(window_sizes, values, sync_tol, max_capacity=1000):

    # logger.debug(sync_tol_arr)
    # logger.debug(battery_life_expected)
    # logger.debug(energy_per_window_arr)
    # zero = battery_life_expected == 0
    # battery_life_expected = battery_life_expected[np.logical_not(zero)]
    # sync_tol_arr = sync_tol_arr[np.logical_not(zero)]
    

    fig, (ax1, ax_split) = plt.subplots(1,2,sharey=True, gridspec_kw={'width_ratios':[15,1]})
    fig.subplots_adjust(wspace=0)

    colors = ['r','black','b']
    markers = ['o', '*', 's']

    for i, value_set in enumerate(values):


        if i==0:
            label='0th Order Rate Adjust'
        elif i==1:
            label = '1st Order Rate Adjust'
        elif i==2: 
            label = '2nd Order Rate Adjust'
        else:
            label = '%dth Order Rate Adjust' % i


        ax1.loglog(window_sizes[:-1], value_set[:-1], colors[i], label=label, marker=markers[i])
        ax1.set_xlabel('Active Period $\mathregular{T_{on}}$ (s)')
        ax1.set_ylabel(f'Battery Life of {max_capacity} mAh battery (days)')

        ax_split.plot(0, value_set[-1], colors[i], marker=markers[i])
        
    ax_split.tick_params(axis="y", left=False, labelleft=False)
    ax1.spines['right'].set_visible(False)
    ax_split.spines['left'].set_visible(False)
    # ax_split.yaxis.tick_left()
    ax_split.tick_params(left=False, labelleft=False, labelsize=16)
    ax_split.yaxis.set_visible(False)
    ax_split.xaxis.set_ticks([0]) #the middle; it thinks we have pixels for 0 and 1
    print(ax_split.xaxis.get_ticklabels())
    ax_split.xaxis.set_ticklabels(['\u221e'])

    ## Draw diagonals to split the axis between finite and infinite window size plots
    kwargs = dict(transform=ax_split.transAxes, color='k', clip_on=False)  # switch to the bottom axes
    ax_split.plot((-.2, -.001), (-.03, .03), **kwargs)  # bottom-left diagonal
    # ax_split.plot((.175, .6), (1-.025, 1+.02), **kwargs)  # bottom-left diagonal

    title = 'Battery Life Estimates vs. Active Period Durations'
    title += '\nSynchronization Accuracy = %.2e seconds' % sync_tol

    formatter = LogFormatter(labelOnlyBase=False, minor_thresholds=(3, 0.4))
    # formatter = LogFormatter(labelOnlyBase=False)
    ax1.yaxis.set_minor_formatter(formatter)
    ax1.yaxis.set_major_formatter(formatter)

    ax1.set_title(title)
    ax1.legend(loc='lower right')

    plt.show()

def plot_power_usage_curves(sync_tol_arr, values, max_capacity=1000, window_size=None):
    colors = ['r','black','b']

    _, ax1 = plt.subplots()
    max_non_inf = 0
    min_val = math.inf
    for i, value_set in enumerate(values):
        max_non_inf = max(np.max(value_set[value_set != math.inf]),max_non_inf)
        min_val = min(np.min(value_set), min_val)
        print(max_non_inf)
    for i, value_set in enumerate(values):
        value_set[value_set == math.inf] = sys.maxsize

        if i==0:
            label='0th Order Rate Adjust'
        elif i==1:
            label = '1st Order Rate Adjust'
        elif i==2: 
            label = '2nd Order Rate Adjust'
        else:
            label = '%dth Order Rate Adjust' % i

        # ax1.semilogx(sync_tol_arr, value_set*1000, colors[i], label=label)
        ax1.loglog(sync_tol_arr, value_set*1000, colors[i], label=label)
        ax1.set_xlabel('Synchronization Accuracy (s)')
        ax1.set_ylabel(f'Average Power (mW)')


        title = 'Average Power for Rate Adjustment Strategies'

        # if show_avg_power:

        #     power = max_capacity*3.3*3.6 / (value_set * 86400)
        #     print(power)
        #     ax2.loglog(sync_tol_arr, power, colors[i])


    if window_size and not window_size == math.inf:
        title += '\nWindow Size = %01.0f seconds' % window_size
    elif window_size and window_size == math.inf:
        title += '\nWindow Size = \u221e seconds'

    # ax1.set_ylim(0, max_non_inf*1000*1.1) 
    ax1.set_ylim(min_val*1000*.75, max_non_inf*1000*1.1) 
    # from matplotlib.ticker import FormatStrFormatter
    formatter = LogFormatter(labelOnlyBase=False, minor_thresholds=(3, 0.4))
    # formatter = LogFormatter(labelOnlyBase=False)
    ax1.yaxis.set_minor_formatter(formatter)
    ax1.yaxis.set_major_formatter(formatter)

    plt.title(title) 
    ax1.legend(loc='right')

    plt.show()


def find_and_plot_holdover_points(battery_life, num_windows, energy_per_window, window_sizes, sync_tol, comparison_points=[], battery_capacity=1000, sleep_current=0):

    inactive_duration = np.logspace(-1,5, num=300)

    fig, ax = plt.subplots()

    for cp in comparison_points:
        ra = cp[0]
        sync_ind = cp[1]
        win_ind = cp[2]
        color = cp[3]
        marker = cp[4]

        sync_acc = sync_tol[sync_ind]
        win_size = window_sizes[win_ind]

        bl = battery_life[ra, sync_ind, win_ind]
        nw = num_windows[ra, sync_ind, win_ind]
        epw = energy_per_window[ra, sync_ind, win_ind]

        if bl == np.zeros(bl.shape):
            continue

        bl_no_duty = battery_life[ra, sync_ind, -1]
        nw_no_duty = num_windows[ra, sync_ind, -1]
        epw_no_duty = energy_per_window[ra, sync_ind, -1]

        ##account for time spent in sleep state between windows
        avg_cycle_current = (epw*3.6 + inactive_duration*sleep_current) / (inactive_duration + win_size)
        lifetime_with_sleep = battery_capacity * 3.6 / avg_cycle_current / 86400

        lifetime = nw * (win_size + inactive_duration)/86400

        if ra==0:
            label='0th Order RA'
        elif ra==1:
            label = '1st Order RA'
        elif ra==2: 
            label = '2nd Order RA'
        else:
            label = '%dth Order RA' % ra


        # ax.loglog(inactive_duration, lifetime, color=color, marker=marker, label=label)
        ax.loglog(inactive_duration, lifetime_with_sleep, color=color, marker=marker, label=label)
        ax.axhline(bl_no_duty, color=color)

        above_ind_lower = np.argwhere(lifetime_with_sleep > bl_no_duty)[0]
        below_ind_upper  = np.argwhere(lifetime_with_sleep < bl_no_duty)[-1]
        holdover_point = (inactive_duration[above_ind_lower] + inactive_duration[below_ind_upper]) / 2
        ax.axvline(holdover_point, linestyle='dashed', color=color)

    plt.plot(inactive_duration[0], 100, marker='None', color='grey', label='Non Duty-Cycled Lifetime')
    plt.ylabel('Battery Life (days)')
    plt.xlabel('Inactive Period $\mathregular{T_{off}}$ (s)')


    title =  'Duty-Cycled Battery Life for $\mathregular{T_{on}}$=%.0f s, $\mathregular{\Pi}$=%.2e s' % (win_size, sync_acc)
    plt.title(title)

    plt.legend(loc='upper left')




def extrapolate_battery_life(battery, application, total_time_passed, only_include_useful_life=False):
    if application.infeasible:
            return 0, math.inf


    if application.W == math.inf:
        logger.info('Calculate battery life for infinite duration sync to precision %e' % application.sync_tolerance)
        energy_per_window = battery.starting_capacity - battery.min_usable_capacity
        logger.info('Energy per window in mAh: %e', energy_per_window)
        used_capacity_mah = battery.starting_capacity - battery.capacity
        averaged_current = (used_capacity_mah*3600/1000) / (total_time_passed-application.inter_window_period)
        #how much energy will be used in total over average energy gives how long (secconds) battery will last
        extrapolated_lifetime = (energy_per_window*3.6) / averaged_current 
        extrapolated_lifetime /= 86400 #in days


    else:
        logger.info('Calculate battery life for %f duration sync to precision %e' % (application.W, application.sync_tolerance))

        if application.windows_used==0:
            return 0, math.inf #this was an infeasible application

        energy_per_window = (battery.starting_capacity - battery.capacity) / application.windows_used #in mAh
        logger.info('Energy per window in mAh: %e; calculated from %d' % (energy_per_window, application.windows_used))
        time_per_window = application.W
        if not only_include_useful_life:
            time_per_window += application.inter_window_period

        extrapolated_lifetime = ((battery.starting_capacity - battery.min_usable_capacity) / energy_per_window) * (time_per_window)
        extrapolated_lifetime = extrapolated_lifetime / 86400

    return extrapolated_lifetime, energy_per_window


def round_to_precision(number, precision):
    number_int = round(number / precision)
    number_rounded = number_int*precision
    return number_rounded