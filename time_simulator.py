import os, sys, math
import simpy
import numpy as np
import random
from matplotlib import pyplot as plt
import logging
from Error import BatteryDeadError, SynchronizationFailed

logging.basicConfig()
main_logger = logging.getLogger('time_sim')
main_logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(levelname)s - [%(name)s]:  %(message)s')

fh = logging.FileHandler('time-sim.log', mode='w')
fh.setFormatter(formatter)
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)
main_logger.handlers = [ch]
main_logger.addHandler(fh)
main_logger.propagate = False


main_logger.debug('test init logger')

from Client import ClientClock
from Battery import Battery
from App import Application
from Temperature import TemperatureCurve, TemperatureCycle, TemperatureSensor
from Oscillator import Oscillator
from Radio import SyncRadio
import helpers

logger = logging.getLogger('time_sim.sim')


random.seed(1)
    

def set_logger_levels(level):
    main_logger.setLevel(level)

def set_console_logger_level(level):
    ch.setLevel(level)

def set_file_logger_level(level):
    fh.setLevel(level)


def run_sim(env, application, oscillator, radio, battery, steps_per_second=1, plot_n_windows=0, max_windows=math.inf, order_of_rate_adjust=0, make_animations=False, max_time=math.inf):

    env.min_step_size = 1/steps_per_second

    debug=False

    if not (isinstance(application, Application) and isinstance(oscillator, Oscillator) and isinstance(radio, SyncRadio) and isinstance(battery, Battery)):
       
        raise TypeError()

    if plot_n_windows:
        logger.warning("Timing diagrams will be plotted for each window; note that each must be viewed, and this option cannot be disabled at runtime with keyboard-interrupts or otherwise!!")

    server_clock = env # just use server_clock.now to generate a timestamp
    client_clock = ClientClock(oscillator, application.sync_tolerance, application.inter_window_period, application, radio, order_of_rate_adjust=order_of_rate_adjust)
    # print("Real frequency of the oscillator starts at %f" % oscillator.real_frequency)

    if not client_clock.is_feasible(radio, allow_warm_start=application.do_warm_start):
        logger.warning("Application with sync tolerance %e is not feasible" % client_clock.sync_tolerance)
        application.infeasible=True
        battery.deplete()
        return

    application.calculate_next_window_start()

    logger.info('\nStarting Simulation\n')
    temperature_sensor_process = env.process(client_clock.temperature_adjustment_process(env, battery))

    # do some warm start; try to match the frequency error before we get into a window. May take multiple messages to build up that model

    while application.windows_used < max_windows:
        logger.debug("\n--------\nDawn of a new window\n--------")
        temperature_sensor_process.interrupt()
        
        client_times_window = []
        client_error_estimates_window = []
        server_times_window = []
        sync_times_window = []

        ## client clock is desynchronized to start

        n = radio.num_messages_for_accuracy(application.sync_tolerance, client_clock) # starting from scratch
        if n == math.inf:
            application.infeasible
            battery.deplete()
        time_to_sync_before_window = n*radio.sync_msg_period

        #calculate duration of time to wait for next window's start, sans however long it takes to sync
        # window_times.server.append(server_clock.now)
        delay_duration = application.next_window_start - server_clock.now - time_to_sync_before_window
        assert delay_duration>0, f'start next window at {application.next_window_start} \t current time is {server_clock.now}, \t and we need {time_to_sync_before_window} s leeway prior'

        # Wait until that period ends; clock is still off, so no need to use the min-step-size
        yield env.timeout(delay_duration) 

        battery.spend_energy(application.app_current_else, delay_duration, server_clock.now)

        # window_times.server.append(server_clock.now)
        # logger.debug('f_multiplier %e before window' %client_clock.frequency_multiplier)


        #Do the initial synchronization
        logger.debug('Presync')
        sync_process = env.process(radio.sync(env, client_clock, battery, presync=True))
        client_times, client_estimates, server_times, err, sync_times = yield sync_process


        #add some info for the output plots for later
        animation_ind = 0
        previous_len = len(client_times_window)
        client_times_window.extend(client_times)
        client_error_estimates_window.extend(client_estimates)
        server_times_window.extend(server_times)
        sync_times_window.extend(sync_times)

        if make_animations:
            for sync_t in sync_times:
                logger.debug(sync_t)
                filename = './animation_plots/RA' + str(order_of_rate_adjust) + '/img_%03d.png' %  animation_ind
                helpers.plot_window(client_times_window[:sync_t[0]], client_error_estimates_window[:sync_t[0]], server_times_window[:sync_t[0]], application.sync_tolerance, application.windows_used, window_start_time=None, save_filename = filename)
                animation_ind += 1

        if isinstance(err, SynchronizationFailed):
            logger.error(client_clock)
            logger.error("Failed within presynchronization; error bounds violated")
            # helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, client_clock.sync_tolerance, application.windows_used, window_start_time=application.next_window_start)
            raise(err)

        if debug:
            logger.debug('Plotting results of presync')
            helpers.plot_window(client_times, client_estimates, server_times, client_clock.sync_tolerance, application.next_window_start)

        # print(application.next_window_start)

        logger.info('\n\tStart Window %d' % (application.windows_used+1))
        window_start = server_clock.now
        window_end = window_start + application.W
        # time_to_next_sync = client_clock.calculate_time_until_next_sync(radio) + server_clock.now
        ## assume that we will wait until we are just about to expire, then we'll exchange one message with the server to stay in sync
        time_to_next_sync = client_clock.time_to_expire() + server_clock.now - 1*radio.sync_msg_period

        logger.debug('Time of next synchronization: %f' % time_to_next_sync)
        while server_clock.now < window_end:

            # logger.debug(f"client: {client_clock.get_time()} \t server_clock: {server_clock.now}\t diff: {server_clock.now - client_clock.get_time()}\t true_diff:{server_clock.now - client_clock.now}")

            remaining_time_til_sync = time_to_next_sync - server_clock.now
            #wait until next synchronization. To avoid phase errors with reading discretized client clock and non-discretized server clock, this needs to be done in integer multiples of a minimum step size. The client clock may also modify the time_to_next_sync due to temperature readings (if 2nd order)
            while remaining_time_til_sync > env.min_step_size and window_end - server_clock.now > env.min_step_size:

                if remaining_time_til_sync < 0:
                    logger.error("Remaining time until the next synchronization is a negative number: %f" % remaining_time_til_sync)
                    logger.warning(client_clock)
                    helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, application.sync_tolerance, application.windows_used+1, window_start_time=application.next_window_start)
                    return
                remaining_time_til_sync = max(remaining_time_til_sync/2, 2*env.min_step_size) #cut the time to wait short so we can poll for temperature update; exponential dropoff. Ugly code.
                delay_duration = min(remaining_time_til_sync, window_end - server_clock.now)

                client_times, client_estimates, server_times = yield env.process(helpers.wait(delay_duration, server_clock, client_clock, battery, sooner_than_later=True))
                client_times_window.extend(client_times)
                client_error_estimates_window.extend(client_estimates)
                server_times_window.extend(server_times)

                if client_clock.get_temp_updated():
                    client_clock.clear_temp_updated()
                    #may see a bend in the error estimate, esp. at high or low temperatures
                    time_to_next_sync = client_clock.time_to_expire() + server_clock.now - radio.sync_msg_period

                remaining_time_til_sync = time_to_next_sync - server_clock.now
                logger.debug(f'remaining_time_til_sync: {remaining_time_til_sync}')

            if window_end - server_clock.now <= radio.sync_msg_period:
                logger.debug('Ending window early; it ends before we haeve time for another synchronization exchange')
                break
        

            logger.debug('Do Sync mid-window')
            sync_process = env.process(radio.sync(env, client_clock, battery))
            # sync_process = env.process(radio.sync(env, client_clock, battery))
            client_times, client_estimates, server_times, err, sync_times = yield sync_process

            previous_len = len(server_times_window)
            client_times_window.extend(client_times)
            client_error_estimates_window.extend(client_estimates)
            server_times_window.extend(server_times)
            sync_times_window.extend(sync_times)

            if isinstance(err, SynchronizationFailed):
                if env.now/(client_clock.sync_tolerance/1000) < 1e13: #condition is based on float64 precision (1e-16); that eventually manifests as forms of error
                    application.infeasible=True
                    logger.error("Failed within mid-window sync; error bounds violated")
                    logger.error(client_clock)
                    # helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, client_clock.sync_tolerance, application.windows_used, window_start_time=application.next_window_start)

                    raise(err)  
                #otherwise, it's probably an issue with floating point errors. Machine epsilon is 1e-16, and those errors can accumulate such that synchornization rates fail to be generated properly. TODO: find a way to fix this. It's an issue with doing very high-precision time-sync with high values for time. Generally, it would be best to represent time as some form of integer, and apply any corrections or adjustments as fractions. This is a substantial overhaul...
                return 

            logger.debug('Finished Sync\n')
            if make_animations:
                for sync_t in sync_times:
                    logger.debug(sync_t)
                    filename = './animation_plots/RA' + str(order_of_rate_adjust) + '/img_%03d.png' %  animation_ind
                    helpers.plot_window(client_times_window[:previous_len+sync_t[0]], client_error_estimates_window[:previous_len+sync_t[0]], server_times_window[:previous_len+sync_t[0]], application.sync_tolerance, application.windows_used, window_start_time=application.next_window_start, save_filename = filename)
                    animation_ind += 1


            if debug:
                logger.debug('Plotting results of mid-window sync')
                helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, client_clock.sync_tolerance, application.windows_used, window_start_time=application.next_window_start)


            time_to_next_sync = client_clock.calculate_time_until_next_sync(radio) + server_clock.now #TODO: make this start sufficiently early so that accumulate offset will not have us started late
            logger.debug("Sync again at t = %f " % time_to_next_sync)

            logger.debug('client reads %f and server reads %f'% (client_clock.get_time(), server_clock.now))

            if server_clock.now > max_time:
                logger.warning('Hit max time for simulation; call it early and return')
                return

        #expend energy post facto for application itself. 
        # battery.spend_energy(oscillator.on_current + application.app_current_window, application.W, server_clock.now)

        logger.debug('Window Ended\n')
        logger.debug('Client clock state \n%s' % str(client_clock))

        client_clock.turn_off()
        application.windows_used += 1

        if plot_n_windows > 0:
            logger.info('Generating plot for sync tolerance %f' % application.sync_tolerance)
            helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, application.sync_tolerance, application.windows_used, window_start_time=application.next_window_start)
            plot_n_windows -= 1

        if make_animations:
            filename = './animation_plots/RA' + str(order_of_rate_adjust) + '/img_%03d.png' %  animation_ind
            helpers.plot_window(client_times_window, client_error_estimates_window, server_times_window, application.sync_tolerance, application.windows_used, window_start_time=application.next_window_start, save_filename = filename)
        
        application.calculate_next_window_start()

        # client_time_per_window.append(window_times['client'])
        # client_time_estimate_per_window.append(window_times['estimate'])
        # server_time_per_window.append(window_times['server'])
        logger.info("Remaining battery capacity %f mAh after window %d" % (battery.capacity, application.windows_used))


    # return client_time_per_window, client_time_estimate_per_window, server_time_per_window



def main():
    #Setup 
    # osc_low_f_xtal = Oscillator(80-6, 32768, 1.5e-6, temperature_vs_drift_model='quadratic') #quadratic model; tuning fork
    osc_med_f_xtal = Oscillator(20e-6, 12e6, 300e-6, 0, 'cubic') #cubic model; assume AT cut 35˚15' centered around 20 C
    # osc_high_f_xtal = Oscillator(20e-6, 40e6, 2.5e-3, temperature_vs_drift_model='cubic', allow_random=True) #cubic temperature model; also AT cut

    # osc_low_f_xtal_ctrl = Oscillator(80e-6, 32768, 1.5e-6, temperature_vs_drift_model='quadratic', allow_random=False)

    dist_from_gateway = 1000
    t_prop = dist_from_gateway / 3e8
    # lora_radio = SyncRadio(80e-3, 5e-3, 1e3, 1e3, 500e3, 600e-9, 1, propagation_time=t_prop) #1 second delay for the sake of LoRaWAN, but not required...
    lora_radio = SyncRadio(80e-3, 5e-3, 600e-3, 150e-3, 500e3, 600e-9, 1, propagation_time=t_prop) #1 second delay for the sake of LoRaWAN, but not required...

    test_application = Application(math.inf, 1e-4, inter_window_period=1000, do_warm_start=True)

    app = test_application
    battery = Battery(1000, stop_early_capacity=985)
    osc = osc_med_f_xtal

    env = simpy.Environment()
    # sim_process = env.process(run_sim(env, app, osc_high_f_xtal, lora_radio, battery, plot_n_windows=0, max_windows=50, steps_per_second=100, order_of_rate_adjust=0))
    sim_process = env.process(run_sim(env, app, osc, lora_radio, battery, plot_n_windows=2, max_windows=math.inf, steps_per_second=100, order_of_rate_adjust=0, make_animations=True))
    try:
        env.run(until=sim_process)
        # x, y, z = yield sim_process
        logger.warning('\n\nSimulation exited')
    except (SynchronizationFailed) as e: #used for telling that the battery has died or exit for some other valid reason
        logger.error(e)
        logger.warning(f'Sync tol was {app.sync_tolerance} and window size was {app.W}')

    except (BatteryDeadError):
        logger.info('Battery dead/depleted')

    except BaseException as e:
        logger.error(e)
        logger.warning(f'Sync tol was {app.sync_tolerance} and window size was {app.W}')
        raise e

    extrapolated_lifetime, average_energy_per_window = helpers.extrapolate_battery_life(battery, app, env.now, only_include_useful_life=True)
    # average_current_in_window = average_energy_per_window * 3.6 / app.W
    logger.info(f"Results for sync tolerance {app.sync_tolerance}")
    logger.info(f"Estimated battery lifetime: {extrapolated_lifetime} days")
    logger.info(f"Energy per window: {average_energy_per_window} mAh\n")


if __name__ == "__main__":
    # main()
    main()



